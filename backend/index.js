const express = require('express')
const morgan = require('morgan')
const cors = require('cors');
const app = express();

const {mongoose} = require('./database')
//Settings
app.set('port',process.env.PORT || 3000);

//Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(cors({original: 'http://localhost:4200'}));

//Routes
app.use('/api/jugadores',require('./routes/jugadores.routes'))
app.use('/api/equipos',require('./routes/equipos.routes'))
app.use('/api/torneos',require('./routes/torneos.routes'))


//Starting the server
app.listen(app.get('port'),()=>{
	console.log('Server on port 3000')
})