const Paciente = require('../models/paciente')
const pacienteCtrl = {};

pacienteCtrl.getPacientes =async (req,res)=>{
	const pacientes = await Paciente.find();
	res.json(pacientes);
}

pacienteCtrl.createPaciente = async (req, res, next) => {
  const paciente = new Paciente({
    codigo: req.body.codigo,
    nombre: req.body.nombre,
    apellidoPaterno: req.body.apellidoPaterno,
    apellidoMaterno: req.body.apellidoMaterno,
    edad: req.body.edad,
    tipoSangre: req.body.tipoSangre
  });
  await paciente.save();
  res.json({ status: "Paciente creado" });
};

pacienteCtrl.getPaciente = async (req, res, next) => {
  const { id } = req.params;
  const paciente = await Paciente.findById(id);
  res.json(paciente);
};

pacienteCtrl.editPaciente = async (req, res, next) => {
  const { id } = req.params;
  await Paciente.findByIdAndUpdate(id, {$set: req.body}, {new: true});
  res.json({ status: "Paciente actualizado" });
};

pacienteCtrl.deletePaciente = async (req, res, next) => {
  await Paciente.findByIdAndRemove(req.params.id);
  res.json({ status: "Paciente eliminado" });
};

module.exports = pacienteCtrl;