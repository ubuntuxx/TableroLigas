const Equipo = require('../models/equipo');
const equipoCtrl = {};

equipoCtrl.getEquipos =async (req,res)=>{
	const equipos = await Equipo.find();
	res.json(equipos);
}

equipoCtrl.createEquipo = async (req, res, next) => {
  const equipo = new Equipo({
    nombre: req.body.nombre,
    numeroJugadores: req.body.numeroJugadores
  });
  await equipo.save();
  res.json({ status: "equipo creado" });
};

equipoCtrl.getEquipo = async (req, res, next) => {
  const { id } = req.params;
  const equipo = await Equipo.findById(id);
  res.json(equipo);
};

equipoCtrl.editEquipo = async (req, res, next) => {
  const { id } = req.params;
  await Equipo.findByIdAndUpdate(id, {$set: req.body}, {new: true});
  res.json({ status: "Equipo actualizado" });
};

equipoCtrl.deleteEquipo = async (req, res, next) => {
  await Equipo.findByIdAndRemove(req.params.id);
  res.json({ status: "Equipo eliminado" });
};

module.exports = equipoCtrl;