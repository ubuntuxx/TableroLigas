const Torneo = require('../models/torneos');
const ContrladorTor = {};

ContrladorTor.getTorneos = async(req, res) => {
    const torneos = await Torneo.find();
    res.json(torneos);
}

ContrladorTor.crearTorneo = async(req, res) => {
    const torneo = new Torneo({
        nombre: req.body.nombre,
        fechaInicio: req.body.fechaInicio,
        fechaCierre: req.body.fechaCierre
      });
      await torneo.save();
      res.json({ status: "Torneo creado" });
}

ContrladorTor.getTorneo = async (req, res) => {
    const { id } = req.params;
    const torneo = await Torneo.findById(id);
    res.json(torneo);
}

ContrladorTor.editTorneo = async (req, res) => {
    const { id } = req.params;
    await Torneo.findByIdAndUpdate(id, {$set: req.body}, {new: true});
    res.json({ status: "Torneo actualizado" });
}

ContrladorTor.deteleTorneo = async (req, res) => { 
    await Torneo.findByIdAndRemove(req.params.id);
    res.json({ status: "Torneo eliminado" });
}

ContrladorTor.editTorneo = async (req, res) => {
    const { id } = req.params;
    await Torneo.findByIdAndUpdate(id, {$set: req.body}, {new: true});
    res.json({ status: "Torneo actualizado" });
}

module.exports = ContrladorTor;