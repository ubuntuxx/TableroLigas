const equipo = require('../models/equipo');
const Jugador = require('../models/jugador')
const jugadorCtrl = {};

jugadorCtrl.getJugadores =async (req,res)=>{
	const jugadores = await Jugador.find();
	res.json(jugadores);
}


jugadorCtrl.createJugador = async (req, res, next) => {
  const jugador = new Jugador({
    nombre: req.body.nombre,
    apellidos: req.body.apellidos,
    edad: req.body.edad,
    estatura: req.body.estatura,
    peso: req.body.peso,
    numero: req.body.numero,
    equipo: req.body.equipo
  });
  await jugador.save();
  res.json({ status: "jugador creado" });
};

jugadorCtrl.getJugador = async (req, res, next) => {
  const { id } = req.params;
  const jugador = await Jugador.findById(id).populate('equipo')
  res.json(jugador);
};

jugadorCtrl.editJugador = async (req, res, next) => {
  const { id } = req.params;
  await Jugador.findByIdAndUpdate(id, {$set: req.body}, {new: true});
  res.json({ status: "Jugador actualizado" });
};

jugadorCtrl.deleteJugador = async (req, res, next) => {
  await Jugador.findByIdAndRemove(req.params.id);
  res.json({ status: "Jugador eliminado" });
};

module.exports = jugadorCtrl;