const {Schema,model} = require('mongoose')
var Torneos = new Schema({
	nombre:{
		type:String
	},
    fechaInicio:{
		type: Date
	}, 
    fechaCierre: {
        type: Date
    }
})
module.exports = model('Torneo',Torneos)
