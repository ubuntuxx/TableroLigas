const {Schema,model} = require('mongoose')
var EquipoSchema = new Schema({
	nombre:{
		type:String
	},
	numeroJugadores:{
		type:Number
	}
})
module.exports = model('Equipo',EquipoSchema)