const {Schema,model} = require('mongoose')
var JugadorSchema = new Schema({
	nombre:{
		type:String,
	},
	apellidos:{
		type:String
	},
	edad:{
		type:Number
	},
    estatura:{
		type:Number
	},
    peso:{
		type:Number
	},
	numero:{
		type:Number
	},
    equipo:{
        type: Schema.Types.ObjectId, ref: "Equipo"
    }
	
})
module.exports = model('Jugador',JugadorSchema)