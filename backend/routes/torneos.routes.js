const express = require('express')
const router = express.Router();

const ContrladorTor = require('../controllers/torneos.controller');

router.get('/',ContrladorTor.getTorneos);

router.post("/", ContrladorTor.crearTorneo);

router.get("/:id", ContrladorTor.getTorneo);

router.put("/:id", ContrladorTor.editTorneo);

router.delete("/:id", ContrladorTor.deteleTorneo);

module.exports = router;
