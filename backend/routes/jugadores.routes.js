const express = require('express')
const router = express.Router();

const jugadorCtrl = require('../controllers/jugador.controller');

router.get('/',jugadorCtrl.getJugadores);

router.post("/", jugadorCtrl.createJugador);

router.get("/:id", jugadorCtrl.getJugador);

router.put("/:id", jugadorCtrl.editJugador);

router.delete("/:id", jugadorCtrl.deleteJugador);

module.exports = router;
